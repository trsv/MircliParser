﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MircliParser.net;
using MircliParser.io;
using MircliParser.data;
using System.Net.Http;
using MircliParser.net.donor;
using System.IO;
using Noesis.Drawing.Imaging.WebP;
using System.Drawing.Imaging;

namespace MircliParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        async private void buttonGo_Click(object sender, EventArgs e)
        {
            try
            {
                DonorHandler handler = new DonorHandler();

                var progress = new Progress<DonorHandler>();
                progress.ProgressChanged += (o, task) =>
                {
                    progressBar.Value = task.progressPercent;
                    progressBar.Update();

                    progressLabel.Text = task.progressMessage;
                };

                buttonGo.Enabled = false;
                progressBar.Visible = true;
                progressLabel.Visible = true;

                await handler.process(progress);

                buttonGo.Enabled = true;
            }
            catch (Exception ex)
            {
                buttonGo.Enabled = true;
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            ProductIO.makeNewFile();
            button1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private async void button3_Click(object sender, EventArgs e)
        {
            Dictionary <string, Product> price = toMap(ProductIO.ReadXlsx("price-1.xlsx"));
            List<Product> prod = ProductIO.ReadXlsx("prod.xlsx");

            int count = 0;

            prod.ForEach(product => 
            {
                if (string.IsNullOrWhiteSpace(product.purchase_rub))
                {
                    string search_key = product.product_name.clear(@"[^a-zA-Zа-яА-Я0-9]+").ToLower();

                    Product new_prod = contains(search_key, ref price);

                    if (new_prod != null)
                    {
                        product.purchase_rub = new_prod.purchase_rub;
                        //product.product_name = new_prod.product_name;
                        count++;
                        //Console.WriteLine(new_prod.product_name);
                    }
                }
            });

            Console.WriteLine("Count: {0}", count);
            Console.WriteLine(price.Count);

            //ProductIO.WriteXslx(prod, "prod-test-new.xlsx");





            //List<Product> images = ProductIO.ReadXlsx("images.xlsx");
            //Dictionary<string, Product> pro = toMap(ProductIO.ReadXlsx("pro.xlsx"));
            //List<Product> dest = new List<Product>();

            //foreach (Product p in images)
            //{
            //    Product product = p;

            //    if (!product.url.Contains("pro-komfort.com"))
            //    {
            //        dest.Add(product);
            //        continue;
            //    }

            //    product.product_full_image1 = pro[product.product_name].product_full_image1.Replace("image_new/", "image/");
            //    product.product_full_image2 = pro[product.product_name].product_full_image2.Replace("image_new/", "image/");
            //    product.product_full_image3 = pro[product.product_name].product_full_image3.Replace("image_new/", "image/");
            //    product.product_full_image4 = pro[product.product_name].product_full_image4.Replace("image_new/", "image/");
            //    product.product_full_image5 = pro[product.product_name].product_full_image5.Replace("image_new/", "image/");

            //    dest.Add(product);
            //}

            //ProductIO.WriteXslx(dest, "prod-test.xlsx");






            //int count_all, count_without, count_with;
            //count_all = count_with = count_without = 0;

            //List<Product> clear = ProductIO.ReadXlsx("prod.xlsx");

            //clear.ForEach(p => 
            //{
            //    if (p.url.Contains("pro-komfort.com"))
            //    {
            //        if (string.IsNullOrWhiteSpace(p.url_yandex)) count_without++;
            //        else count_with++;
            //        count_all++;
            //    }

            //});

            //Console.WriteLine("Товаров всего: {0}", count_all);
            //Console.WriteLine("Товаров без маркета: {0}", count_without);
            //Console.WriteLine("Товаров с маркетом: {0}", count_with);





            //List<Product> clear = ProductIO.ReadXlsx("clear.xlsx");

            //Dictionary<string, Product> mircli = toMap(ProductIO.ReadXlsx("mircli.xlsx"));
            //Dictionary<string, Product> tecl = toMap(ProductIO.ReadXlsx("tecl.xlsx"));
            //Dictionary<string, Product> mir_pro = toMap(ProductIO.ReadXlsx("mir_pro.xlsx"));

            //List<Product> new_list = new List<Product>();

            //bool success = true;

            //foreach (Product p in clear)
            //{
            //    Product product;

            //    if (p.url.Contains("mircli.ru")) product = mircli[p.product_name];
            //    else if (p.url.Contains("tecl.ru")) product = tecl[p.product_name];
            //    else if (p.url.Contains("pro-komfort.com") || p.url.Contains("mirklimata")) product = mir_pro[p.product_name];
            //    else { success = false; break; }

            //    new_list.Add(product);
            //}

            //if (!success)
            //{
            //    MessageBox.Show("Не найдено название товара", "Ошибка");
            //    return;
            //}

            //ProductIO.WriteXslx(new_list, "prod.xlsx");
        }

        private Dictionary<string, Product> toMap(List<Product> src)
        {
            Dictionary<string, Product> list = new Dictionary<string, Product>();

            foreach (Product p in src)
            {
                string new_name = p.product_name.clear(@"[^a-zA-Zа-яА-Я0-9]+").ToLower();
                
                if (!list.ContainsKey(new_name))
                list.Add(new_name, p);
            }

            return list;
        }

        private Product contains(string name_search, ref Dictionary <string, Product> list)
        {
            foreach (KeyValuePair <string, Product> pair in list)
            {
                if (pair.Key.Contains(name_search))
                {
                    Console.WriteLine("{0} = {1} = {2}", name_search, pair.Key, pair.Value.product_name);
                    return pair.Value;
                }
            }

            return null;
        }
    }
}
