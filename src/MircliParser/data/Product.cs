﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MircliParser.data
{
    /// <summary>
    /// Класс представляет объект продукта
    /// </summary>
    class Product
    {
        public string url { get; set; }
        public string product_s_desc { get; set; } // краткое описание товара
        public string product_desc { get; set; } // полное описание товара
        public string product_name { get; set; } // название продукта
        public string price_with_discount { get; set; } // цена из файла
        public string product_currency { get; set; } // валюта 
        public string product_full_image1 { get; set; }
        public string product_full_image2 { get; set; }
        public string product_full_image3 { get; set; }
        public string product_full_image4 { get; set; }
        public string product_full_image5 { get; set; }
        public string category_path { get; set; } // значение из файла (Кондиционеры_пронумерованы категории_НОЯБРЬ_31_число) колонка F
        public string category_text { get; set; } // категория текстом из файла  (Кондиционеры_пронумерованы категории_НОЯБРЬ_31_число) колонка С
        public string category_type { get; set; } // тип категории из файла  (Кондиционеры_пронумерованы категории_НОЯБРЬ_31_число) колонка D
        public string brend { get; set; } // значение из файла (Кондиционеры_пронумерованы категории_НОЯБРЬ_31_число) колонка E
        public string contry_brand { get; set; } // родина бренда
        public string Manufacturer_country { get; set; } // страна изготовитель
        public string purchase_dol{ get; set; } // закупка в $ (заполняем из файла прайс) если в файле нету то из сайта
        public string purchase_rub { get; set; } // закупка в руб (заполняем из файла прайс) если в файле нету то из сайта        
        public string sale { get; set; } // данные ячейки заполняются из файла "Прайс" если товар отсутствует в ПРАЙС - данные заполнить из донора
        public string split_multy { get; set; } //        Тип  (сплит/мультисплит) данные с донора или с ЯнМаркета
        public string Indoor_unit_type { get; set; } // Тип внутренего тока, (настенный/ потолочный/ кассетный/ канальный/ колонный) данные с донора или с ЯнМаркета
        public string basic_model { get; set; } //     Основные режимы         (охлаждение / охлаждение обогрев) данные с донора или с ЯнМаркета
        public string Inverter { get; set; } // Инвертор (плавная регулировка мощности)Y/N, данные с донора или с ЯнМаркета
        public string Cooling_powerKBT { get; set; } // мощность Охлаждения кВТ
        public string Cooling_powerBTE { get; set; } // мощность Охлаждения  BTE
        public string heating_capacityKBT { get; set; } // мощность обогрева кВТ
        public string heating_capacityBTE { get; set; } // мощность обогрева  BTE   
        public string square { get; set; } // Обслуживаемая площадь до 20/ 21-30/ 31-54/ 55-79/ 80-120/  более 120 данные с донора или с ЯнМаркета
        public string category_powerBTU { get; set; } // Категория мощности, BTU (7,9,12,18,24….)
        public string Remote_control { get; set; } //   Пульт дистанционного управления Y/N
        public string Night_mode { get; set; } // Ночной режим Y/N
        public string avto_temperatures { get; set; } // Автоматическое поддержание температуры  Y/N  данные с донора или с ЯнМаркета
        public string air_ventilation_mode { get; set; } // Режим приточной вентиляции        Y/N
        public string Dehumidification_mode { get; set; } //   Режим осушения воздуха
        public string Fine_filter { get; set; } // Фильтр тонкой очистки Y/N
        public string Number_indoor_units{ get; set; } // Число внутренних блоков 2/3/4/5/6/7/8
        public string Autoreset { get; set; } // Автоперезапуск  Y/N данные с донора или с ЯнМаркета
        public string Plasma_filter { get; set; } // Плазменный фильтр  Y/N
        public string Motion_Sensor { get; set; } //   Датчик движения Y/N
        public string Wi_Fi { get; set; } // Wi-Fi Y/N
        public string Maximum_air_flow { get; set; } // Максимальный воздушный поток  куб.м./мин
        public string Timer { get; set; } //   Таймер включения/выключения
        public string Self_diagnosis { get; set; } // Самодиагностика неисправностей
        public string Deodorizing_filter { get; set; } // Дезодорирующий фильтр
        public string Filter_with_vitamin_C { get; set; } //    Фильтр с витамином С данные с донора или с ЯнМаркета
        public string Anion_generator { get; set; } // Генератор анионов
        public string Adjusting_air_flow { get; set; } // Регулировка возд потока
        public string Number_of_fan_speeds { get; set; } // Количество скоростей вентилятора
        public string Dehumidification_intensity { get; set; } // Интенсивность осушения  (л/час)
        public string Ventilation_mode { get; set; } // Режим вентиляции (без охлаждения и обогрева) Y/N данные с донора или с ЯнМаркета
        public string Cooling_power_consumption { get; set; } // Потребляемая мощность при охлаждении
        public string Power_consumption_for_heating { get; set; } // Потребляемая мощность при обогреве
        public string Minimum_noise_level { get; set; } // Минимальный уровень шума
        public string Maximum_noise_level { get; set; } // Максимальный уровень шума
        public string Dimensions_indoor_unit_split_system { get; set; } // Габариты внутреннего блока сплит-системы (ШxВxГ)
        public string Dimensions_outdoor_unit_split_system { get; set; } // Габариты наружного блока сплит-системы (ШxВxГ)
        public string Maximum_length_of_communications { get; set; } // Максимальная длина коммуникаций
        public string Minimum_room_temperature { get; set; } // Минимальная температура в помещении
        public string Maximum_room_temperature { get; set; } // Максимальная температура в помещении
        public string Minimum_temperature_in_cooling_mode { get; set; } // Минимальная температура для эксплуатации кондиционера в режиме охлаждения
        public string Minimum_temperature_in_heating_mode { get; set; } // Минимальная температура для эксплуатации кондиционера в режиме обогрева
        public string Static_pressure { get; set; } // Статическое давление
        public string Phase { get; set; } // Фаза 220/380
        public string Weight_outdoor { get; set; } // Вес наружного
        public string Weight_of_inner { get; set; } // Вес внутреннего  
        public string Display_indoor_unit { get; set; } // Дисплей на внутреннем блоке
        public string Power_supply { get; set; } // Подача питани наружный/внутренний
        public string Refrigerant { get; set; } // Хладагент
        public string Air_exchange { get; set; } // Воздухообмен
        public string Air_consumption { get; set; } // Расход воздуха 
        public string Noise_level_external_unit{ get; set; } // Уровень шума, дБа внешний блок
        public string Noise_level_Internal_unit { get; set; } // Уровень шума, дБа Внутренний блок
        public string Temperature_adjustment { get; set; } // Регулировка температуры 
        public string Fan_Performance { get; set; } // Производительность вентилятора 
        public string url_yandex { get; set; }  // 
        public string EER { get; set; } // энергоэффективность (охлаждение)
        public string COP { get; set; } // энергоэффективность (обогрев)
        public string class_energy { get; set; } // класс энергопотребления
    }
}
