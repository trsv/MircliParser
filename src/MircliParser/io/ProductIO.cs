﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MircliParser.data;
using OfficeOpenXml;
using System.IO;

namespace MircliParser.io
{
    /// <summary>
    /// Класс для ввода/вывода продуктов 
    /// </summary>
    class ProductIO
    {
        private const string DEFAULT_FILENAME_IN = @"in.xlsx";  // путь входного файла
        private const string DEFAULT_FILENAME_OUT = @"out.xlsx";// путь входного файла
        private const string PATH_SHORT = @"short.xlsx";        // путь к 'Краткое описание...'
        private const string PATH_PRICE = "price.xlsx";         // путь к 'Прайс...'
        private const string PATH_SOURCE = @"source.xlsx";      // путь к ненужному файлу
        private const string PATH_SPLIT = @"split.xlsx";        // путь к 'Кондиционеры...'




        /// <summary>
        /// Чтение файла "прайс"
        /// </summary>
        /// <returns>Список название-продукт</returns>
        private static Dictionary<string, Product> readPriceList()
        {
            Dictionary<string, Product> list = new Dictionary<string, Product>();
            Console.WriteLine("[INFO] Читаем 'Прайс' из: {0}", PATH_PRICE);

            using (FileStream fileStream = new FileStream(PATH_PRICE, FileMode.Open))
            {
                ExcelPackage excel = new ExcelPackage(fileStream);
                var sheet = excel.Workbook.Worksheets.First();

                for (int row = 2; row <= sheet.Dimension.End.Row; row++)
                {
                    string title, pricePurchaseDol, pricePurchaseRub, priceSale;
                    title = pricePurchaseDol = pricePurchaseRub = priceSale = string.Empty;

                    try
                    {
                        title = readValue(row, 2, ref sheet);
                        pricePurchaseDol = readValue(row, 4, ref sheet);
                        pricePurchaseRub = readValue(row, 5, ref sheet);
                        priceSale = readValue(row, 6, ref sheet);

                        //if (string.IsNullOrWhiteSpace(price)) price = "0";

                        if (!list.ContainsKey(title) && !string.IsNullOrWhiteSpace(title))
                        {
                            list.Add(title, new Product
                            {
                                product_name = title,
                                sale = priceSale,
                                purchase_dol = pricePurchaseDol,
                                purchase_rub = pricePurchaseRub,
                                product_currency = "RUB"
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[ERROR] {0}", e);
                        Console.WriteLine(title);
                        throw e;
                    }

                }
            }

            Console.WriteLine("[INFO] Прочитано всего: {0}", list.Count);
            return list;
        }


        /// <summary>
        /// Чтение файла "Краткое описание"
        /// </summary>
        /// <returns>Список название-продукт</returns>
        private static Dictionary<string, Product> readShortList()
        {
            Dictionary<string, Product> list = new Dictionary<string, Product>();
            Console.WriteLine("[INFO] Читаем 'Краткое описание' из: {0}", PATH_PRICE);

            using (FileStream fileStream = new FileStream(PATH_SHORT, FileMode.Open))
            {

                Dictionary<string, Product> prices = readPriceList();

                ExcelPackage excel = new ExcelPackage(fileStream);
                var sheet = excel.Workbook.Worksheets.First();

                for (int row = 3; row <= sheet.Dimension.End.Row; row++)
                {
                    string title, priceString, priceDol, priceRub, priceSale, currency, desc;
                    title = priceString = priceDol = priceRub = priceSale = currency = desc = string.Empty;
                    try
                    {
                        title = readValue(row, 1, ref sheet);
                        desc = readValue(row, 3, ref sheet);
                        priceString = readValue(row, 4, ref sheet);

                        if (!string.IsNullOrWhiteSpace(priceString) && (priceString.Contains("USD") || priceString.Contains("RUB")))
                        {
                            currency = priceString.Split(' ')[0];
                            priceSale = priceString.Split(' ')[1];
                        }
                        else
                        {
                            try
                            {
                                priceDol = prices[title].purchase_dol;
                                priceRub = prices[title].purchase_rub;
                                priceSale = prices[title].sale;
                                currency = prices[title].product_currency;
                            }
                            catch (KeyNotFoundException)
                            {
                                priceSale = priceDol = priceRub = currency = string.Empty;
                            }
                        }

                        if (!list.ContainsKey(title) && !string.IsNullOrWhiteSpace(title))
                        {
                            list.Add(title, new Product
                            {
                                product_name = title,
                                product_s_desc = desc,
                                product_currency = currency,
                                purchase_dol = priceDol,
                                purchase_rub = priceRub,
                                sale = priceSale
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[ERROR] {0}", e);
                        Console.WriteLine(title);
                        throw e;
                    }

                }
            }

            Console.WriteLine("[INFO] Прочитано всего: {0}", list.Count);
            return list;
        }


        /// <summary>
        /// Чтение файла "Кондиционеры..."
        /// </summary>
        /// <returns>Список название-продукт</returns>
        private static Dictionary<string, Product> readSplitList()
        {
            Dictionary<string, Product> list = new Dictionary<string, Product>();
            Console.WriteLine("[INFO] Читаем 'Кондиционеры...' из: {0}", PATH_SPLIT);

            using (FileStream fileStream = new FileStream(PATH_SPLIT, FileMode.Open))
            {

                Dictionary<string, Product> shorts = readShortList();

                ExcelPackage excel = new ExcelPackage(fileStream);
                var sheet = excel.Workbook.Worksheets.First();

                for (int row = 2; row <= sheet.Dimension.End.Row; row++)
                {
                    string title, fUrl, category, type, brand, id, currency, priceRub, priceDol, priceSale, desc;
                    title = fUrl = category = type = brand = id = currency = priceRub = priceDol = priceSale = desc = string.Empty;

                    try
                    {
                        title = readValue(row, 2, ref sheet);
                        fUrl = readValue(row, 1, ref sheet);
                        category = readValue(row, 3, ref sheet);
                        type = readValue(row, 4, ref sheet);
                        brand = readValue(row, 5, ref sheet);
                        id = readValue(row, 6, ref sheet);

                        if (shorts.ContainsKey(title))
                        {
                            currency = shorts[title].product_currency;
                            priceSale = shorts[title].sale;
                            priceDol = shorts[title].purchase_dol;
                            priceRub = shorts[title].purchase_rub;
                            desc = shorts[title].product_s_desc;
                        }

                        if (!list.ContainsKey(title) && !string.IsNullOrWhiteSpace(title))
                        {
                            list.Add(title, new Product
                            {
                                product_name = title,
                                url = fUrl,
                                category_path = id,
                                category_text = category,
                                category_type = type,
                                brend = brand,

                                product_currency = currency,
                                purchase_dol = priceDol,
                                purchase_rub = priceRub,
                                sale = priceSale,
                                product_s_desc = desc
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[ERROR] {0}", e);
                        Console.WriteLine(title);
                        throw e;
                    }

                }
            }

            Console.WriteLine("[INFO] Прочитано всего: {0}", list.Count);
            return list;
        }

        /// <summary>
        /// Чтение значение из ячейки по координатам
        /// </summary>
        /// <param name="row">Строка ячейки</param>
        /// <param name="column">Столбец ячейки</param>
        /// <param name="sheet">Лист в файле</param>
        /// <returns>Значение ячейки или пустая строка, если ячейка пуста</returns>
        private static string readValue(int row, int column, ref ExcelWorksheet sheet)
        {
            string value;

            try
            {
                value = sheet.Cells[row, column].Value.ToString().Trim();
            }
            catch (NullReferenceException)
            {
                value = string.Empty;
            }

            return value;
        }


        /// <summary>
        /// Запись списка продуктов в файл
        /// </summary>
        /// <param name="listInfo">Список продуктов</param>
        /// <param name="path">Путь к файлу</param>
        public static void WriteXslx(List<Product> listInfo, string @path = DEFAULT_FILENAME_OUT)
        {
            FileInfo newFile = new FileInfo(path);

            ExcelPackage excel = new ExcelPackage(newFile);

            if (excel.Workbook.Worksheets.Count == 0) excel.Workbook.Worksheets.Add("Content");
            var sheet = excel.Workbook.Worksheets.First();

            int row = 0;

            if (!newFile.Exists)
            {
                sheet.Cells[1, 1].Value = "product_name\nимя товара";
                sheet.Cells[1, 2].Value = "price_with_discount\nРозничная цена в ВИМТЕХ";
                sheet.Cells[1, 3].Value = "product_currency\nВалюта цены";
                sheet.Cells[1, 4].Value = "product_s_desc\nКраткое описание товара";
                sheet.Cells[1, 5].Value = "product_desc\nПолное описание";
                sheet.Cells[1, 6].Value = "product_full_image_1\n Изображение товара 1";
                sheet.Cells[1, 7].Value = "product_full_image_2\n Изображение товара 2";
                sheet.Cells[1, 8].Value = "product_full_image_3\n Изображение товара 3";
                sheet.Cells[1, 9].Value = "product_full_image_4\n Изображение товара 4";
                sheet.Cells[1, 10].Value = "product_full_image_5\n Изображение товара 5";
                sheet.Cells[1, 11].Value = "category_path\n 6 или 9 значный код Категории";
                sheet.Cells[1, 12].Value = "Категория текстом\n Наименование категории у донора 3";
                sheet.Cells[1, 13].Value = "Тип категории\n Тип категории у ДОНОРА";
                sheet.Cells[1, 14].Value = "Бренд\n Имя производителя у Донора";
                sheet.Cells[1, 15].Value = "Родина бренда";
                sheet.Cells[1, 16].Value = "Страна изготовления";
                sheet.Cells[1, 17].Value = "закуп$";
                sheet.Cells[1, 18].Value = "закупRub";
                sheet.Cells[1, 19].Value = "ПРОДАЖА";
                sheet.Cells[1, 20].Value = "Тип\n (сплит / мультисплит)";
                sheet.Cells[1, 21].Value = "Тип внутреннего блока\n (настенный / потолочный / кассетный / канальный / колонный)";
                sheet.Cells[1, 22].Value = "Основные режимы\n (охлаждение / охлаждение обогрев)";
                sheet.Cells[1, 23].Value = "Инвертор\n (плавная регулировка мощности) Y/N";
                sheet.Cells[1, 24].Value = "мощность Охлаждения\nкВТ";
                sheet.Cells[1, 25].Value = "мощность Охлаждения\nBTE";
                sheet.Cells[1, 26].Value = "мощность обогрева\nкВТ";
                sheet.Cells[1, 27].Value = "мощность обогрева\nBTE";
                sheet.Cells[1, 28].Value = "Обслуживаемая площадь\n до 20 / 21 - 30 / 31 - 54 / 55 - 79 / 80 - 120 / более 120";
                sheet.Cells[1, 29].Value = "Категория мощности, BTU\n (7, 9, 12, 18, 24….)";
                sheet.Cells[1, 30].Value = "Пульт дистанционного управления\n Y/N";
                sheet.Cells[1, 31].Value = "Ночной режим\n Y/N";
                sheet.Cells[1, 32].Value = "Автоматическое поддержание температуры\n Y/N";
                sheet.Cells[1, 33].Value = "Режим приточной вентиляции\n Y/N";
                sheet.Cells[1, 34].Value = "Режим осушения воздуха\n литров / час";
                sheet.Cells[1, 35].Value = "Фильтр тонкой очистки\n Y/N";
                sheet.Cells[1, 36].Value = "Число внутренних блоков\n 2 / 3 / 4 / 5 / 6 / 7 / 8";
                sheet.Cells[1, 37].Value = "Автоперезапуск\n Y/N";
                sheet.Cells[1, 38].Value = "Плазменный фильтр\n Y/N";
                sheet.Cells[1, 39].Value = "Датчик движения\n Y/N";
                sheet.Cells[1, 40].Value = "Wi-Fi\n Y/N";
                sheet.Cells[1, 41].Value = "Максимальный воздушный поток\n куб.м./ мин";
                sheet.Cells[1, 42].Value = "Таймер включения/выключения";
                sheet.Cells[1, 43].Value = "Самодиагностика неисправностей";
                sheet.Cells[1, 44].Value = "Дезодорирующий фильтр";
                sheet.Cells[1, 45].Value = "Фильтр с витамином С";
                sheet.Cells[1, 46].Value = "Генератор анионов";
                sheet.Cells[1, 47].Value = "Регулировка возд потока";
                sheet.Cells[1, 48].Value = "Количество скоростей вентилятора";
                sheet.Cells[1, 49].Value = "Интенсивность осушения\n (л / час";
                sheet.Cells[1, 50].Value = "Режим вентиляции (без охлаждения и обогрева)\n Y/N";
                sheet.Cells[1, 51].Value = "Потребляемая мощность при охлаждении";
                sheet.Cells[1, 52].Value = "Потребляемая мощность при обогреве";
                sheet.Cells[1, 53].Value = "Минимальный уровень шума";
                sheet.Cells[1, 54].Value = "Максимальный уровень шума";
                sheet.Cells[1, 55].Value = "Габариты внутреннего блока сплит-системы\n (ШxВxГ)";
                sheet.Cells[1, 56].Value = "Габариты наружного блока сплит-системы\n (ШxВxГ)";
                sheet.Cells[1, 57].Value = "Максимальная длина коммуникаций";
                sheet.Cells[1, 58].Value = "Минимальная температура в помещении";
                sheet.Cells[1, 59].Value = "Максимальная температура в помещении";
                sheet.Cells[1, 60].Value = "Минимальная температура для эксплуатации кондиционера в режиме охлаждения";
                sheet.Cells[1, 61].Value = "Минимальная температура для эксплуатации кондиционера в режиме обогрева";
                sheet.Cells[1, 62].Value = "Статическое давление";
                sheet.Cells[1, 63].Value = "Фаза\n 220 / 380";
                sheet.Cells[1, 64].Value = "Вес наружного";
                sheet.Cells[1, 65].Value = "Вес внутреннего";
                sheet.Cells[1, 66].Value = "Дисплей на внутреннем блоке";
                sheet.Cells[1, 67].Value = "Подача питания наружный / внутренний";
                sheet.Cells[1, 68].Value = "Url"; 
                sheet.Cells[1, 69].Value = "Хладагент"; 
                sheet.Cells[1, 70].Value = "Воздухообмен"; 
                sheet.Cells[1, 71].Value = "Расход воздуха";
                sheet.Cells[1, 72].Value = "Уровень шума, дБа внешний блок"; 
                sheet.Cells[1, 73].Value = "Уровень шума, дБа внутрений блок";
                sheet.Cells[1, 74].Value = "Регулировка температуры"; 
                sheet.Cells[1, 75].Value = "Производительность вентилятора";
                sheet.Cells[1, 76].Value = "Ссылка яндекса";
                sheet.Cells[1, 77].Value = "EER";
                sheet.Cells[1, 78].Value = "COP";
                sheet.Cells[1, 79].Value = "Класс энергопотребления";
                row = 2;
            }
            else
            {
                row = sheet.Dimension.End.Row + 1;
            }

            for (int i = 0; i < listInfo.Count; row++, i++)
            {
                try
                {
                    sheet.Cells[row, 1].Value = listInfo[i].product_name;
                    sheet.Cells[row, 2].Value = listInfo[i].price_with_discount;
                    sheet.Cells[row, 3].Value = listInfo[i].product_currency;
                    sheet.Cells[row, 4].Value = listInfo[i].product_s_desc;
                    sheet.Cells[row, 5].Value = listInfo[i].product_desc;
                    sheet.Cells[row, 6].Value = listInfo[i].product_full_image1;
                    sheet.Cells[row, 7].Value = listInfo[i].product_full_image2;
                    sheet.Cells[row, 8].Value = listInfo[i].product_full_image3;
                    sheet.Cells[row, 9].Value = listInfo[i].product_full_image4;
                    sheet.Cells[row, 10].Value = listInfo[i].product_full_image5;
                    sheet.Cells[row, 11].Value = listInfo[i].category_path;
                    sheet.Cells[row, 12].Value = listInfo[i].category_text;
                    sheet.Cells[row, 13].Value = listInfo[i].category_type;
                    sheet.Cells[row, 14].Value = listInfo[i].brend;
                    sheet.Cells[row, 15].Value = listInfo[i].contry_brand;
                    sheet.Cells[row, 16].Value = listInfo[i].Manufacturer_country;
                    sheet.Cells[row, 17].Value = listInfo[i].purchase_dol;
                    sheet.Cells[row, 18].Value = listInfo[i].purchase_rub;
                    sheet.Cells[row, 19].Value = listInfo[i].sale;
                    sheet.Cells[row, 20].Value = listInfo[i].split_multy;
                    sheet.Cells[row, 21].Value = listInfo[i].Indoor_unit_type;
                    sheet.Cells[row, 22].Value = listInfo[i].basic_model;
                    sheet.Cells[row, 23].Value = listInfo[i].Inverter;
                    sheet.Cells[row, 24].Value = listInfo[i].Cooling_powerKBT;
                    sheet.Cells[row, 25].Value = listInfo[i].Cooling_powerBTE;
                    sheet.Cells[row, 26].Value = listInfo[i].heating_capacityKBT;
                    sheet.Cells[row, 27].Value = listInfo[i].heating_capacityBTE;
                    sheet.Cells[row, 28].Value = listInfo[i].square;
                    sheet.Cells[row, 29].Value = listInfo[i].category_powerBTU;
                    sheet.Cells[row, 30].Value = listInfo[i].Remote_control;
                    sheet.Cells[row, 31].Value = listInfo[i].Night_mode;
                    sheet.Cells[row, 32].Value = listInfo[i].avto_temperatures;
                    sheet.Cells[row, 33].Value = listInfo[i].air_ventilation_mode;
                    sheet.Cells[row, 34].Value = listInfo[i].Dehumidification_mode;
                    sheet.Cells[row, 35].Value = listInfo[i].Fine_filter;
                    sheet.Cells[row, 36].Value = listInfo[i].Number_indoor_units;
                    sheet.Cells[row, 37].Value = listInfo[i].Autoreset;
                    sheet.Cells[row, 38].Value = listInfo[i].Plasma_filter;
                    sheet.Cells[row, 39].Value = listInfo[i].Motion_Sensor;
                    sheet.Cells[row, 40].Value = listInfo[i].Wi_Fi;
                    sheet.Cells[row, 41].Value = listInfo[i].Maximum_air_flow;
                    sheet.Cells[row, 42].Value = listInfo[i].Timer;
                    sheet.Cells[row, 43].Value = listInfo[i].Self_diagnosis;
                    sheet.Cells[row, 44].Value = listInfo[i].Deodorizing_filter;
                    sheet.Cells[row, 45].Value = listInfo[i].Filter_with_vitamin_C;
                    sheet.Cells[row, 46].Value = listInfo[i].Anion_generator;
                    sheet.Cells[row, 47].Value = listInfo[i].Adjusting_air_flow;
                    sheet.Cells[row, 48].Value = listInfo[i].Number_of_fan_speeds;
                    sheet.Cells[row, 49].Value = listInfo[i].Dehumidification_intensity;
                    sheet.Cells[row, 50].Value = listInfo[i].Ventilation_mode;
                    sheet.Cells[row, 51].Value = listInfo[i].Cooling_power_consumption;
                    sheet.Cells[row, 52].Value = listInfo[i].Power_consumption_for_heating;
                    sheet.Cells[row, 53].Value = listInfo[i].Minimum_noise_level;
                    sheet.Cells[row, 54].Value = listInfo[i].Maximum_noise_level;
                    sheet.Cells[row, 55].Value = listInfo[i].Dimensions_indoor_unit_split_system;
                    sheet.Cells[row, 56].Value = listInfo[i].Dimensions_outdoor_unit_split_system;
                    sheet.Cells[row, 57].Value = listInfo[i].Maximum_length_of_communications;
                    sheet.Cells[row, 58].Value = listInfo[i].Minimum_room_temperature;
                    sheet.Cells[row, 59].Value = listInfo[i].Maximum_room_temperature;
                    sheet.Cells[row, 60].Value = listInfo[i].Minimum_temperature_in_cooling_mode;
                    sheet.Cells[row, 61].Value = listInfo[i].Minimum_temperature_in_heating_mode;
                    sheet.Cells[row, 62].Value = listInfo[i].Static_pressure;
                    sheet.Cells[row, 63].Value = listInfo[i].Phase;
                    sheet.Cells[row, 64].Value = listInfo[i].Weight_outdoor;
                    sheet.Cells[row, 65].Value = listInfo[i].Weight_of_inner;
                    sheet.Cells[row, 66].Value = listInfo[i].Display_indoor_unit;
                    sheet.Cells[row, 67].Value = listInfo[i].Power_supply;
                    sheet.Cells[row, 68].Value = listInfo[i].url; 
                    sheet.Cells[row, 69].Value = listInfo[i].Refrigerant;
                    sheet.Cells[row, 70].Value = listInfo[i].Air_exchange; 
                    sheet.Cells[row, 71].Value = listInfo[i].Air_consumption; 
                    sheet.Cells[row, 72].Value = listInfo[i].Noise_level_external_unit;
                    sheet.Cells[row, 73].Value = listInfo[i].Noise_level_Internal_unit;
                    sheet.Cells[row, 74].Value = listInfo[i].Temperature_adjustment; 
                    sheet.Cells[row, 75].Value = listInfo[i].Fan_Performance;
                    sheet.Cells[row, 76].Value = listInfo[i].url_yandex;
                    sheet.Cells[row, 77].Value = listInfo[i].EER;
                    sheet.Cells[row, 78].Value = listInfo[i].COP;
                    sheet.Cells[row, 79].Value = listInfo[i].class_energy;
                }
                catch (NullReferenceException)
                {
                    break;
                }

            }

            excel.Save();
        }

        public static List<Product> ReadXlsx(string @path = DEFAULT_FILENAME_IN)
        {
            List<Product> list = new List<Product>();

            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                ExcelPackage excel = new ExcelPackage(fileStream);
                var sheet = excel.Workbook.Worksheets.First();

                Console.WriteLine("[INFO] Читам файл с продуктами из {0}", path);

                try
                {
                    for (int row = 2; row <= sheet.Dimension.End.Row; row++)
                    {
                        try
                        {
                            Product p = new Product
                            {
                                product_name = readValue(row, 1, ref sheet),
                                price_with_discount = readValue(row, 2, ref sheet),
                                product_currency = readValue(row, 3, ref sheet),
                                product_s_desc = readValue(row, 4, ref sheet),
                                product_desc = readValue(row, 5, ref sheet),
                                product_full_image1 = readValue(row, 6, ref sheet),
                                product_full_image2 = readValue(row, 7, ref sheet),
                                product_full_image3 = readValue(row, 8, ref sheet),
                                product_full_image4 = readValue(row, 9, ref sheet),
                                product_full_image5 = readValue(row, 10, ref sheet),
                                category_path = readValue(row, 11, ref sheet),
                                category_text = readValue(row, 12, ref sheet),
                                category_type = readValue(row, 13, ref sheet),
                                brend = readValue(row, 14, ref sheet),
                                contry_brand = readValue(row, 15, ref sheet),
                                Manufacturer_country = readValue(row, 16, ref sheet),
                                purchase_dol = readValue(row, 17, ref sheet),
                                purchase_rub = readValue(row, 18, ref sheet),
                                sale = readValue(row, 19, ref sheet),
                                split_multy = readValue(row, 20, ref sheet),
                                Indoor_unit_type = readValue(row, 21, ref sheet),
                                basic_model = readValue(row, 22, ref sheet),
                                Inverter = readValue(row, 23, ref sheet),
                                Cooling_powerKBT = readValue(row, 24, ref sheet),
                                Cooling_powerBTE = readValue(row, 25, ref sheet),
                                heating_capacityKBT = readValue(row, 26, ref sheet),
                                heating_capacityBTE = readValue(row, 27, ref sheet),
                                square = readValue(row, 28, ref sheet),
                                category_powerBTU = readValue(row, 29, ref sheet),
                                Remote_control = readValue(row, 30, ref sheet),
                                Night_mode = readValue(row, 31, ref sheet),
                                avto_temperatures = readValue(row, 32, ref sheet),
                                air_ventilation_mode = readValue(row, 33, ref sheet),
                                Dehumidification_mode = readValue(row, 34, ref sheet),
                                Fine_filter = readValue(row, 35, ref sheet),
                                Number_indoor_units = readValue(row, 36, ref sheet),
                                Autoreset = readValue(row, 37, ref sheet),
                                Plasma_filter = readValue(row, 38, ref sheet),
                                Motion_Sensor = readValue(row, 39, ref sheet),
                                Wi_Fi = readValue(row, 40, ref sheet),
                                Maximum_air_flow = readValue(row, 41, ref sheet),
                                Timer = readValue(row, 42, ref sheet),
                                Self_diagnosis = readValue(row, 43, ref sheet),
                                Deodorizing_filter = readValue(row, 44, ref sheet),
                                Filter_with_vitamin_C = readValue(row, 45, ref sheet),
                                Anion_generator = readValue(row, 46, ref sheet),
                                Adjusting_air_flow = readValue(row, 47, ref sheet),
                                Number_of_fan_speeds = readValue(row, 48, ref sheet),
                                Dehumidification_intensity = readValue(row, 49, ref sheet),
                                Ventilation_mode = readValue(row, 50, ref sheet),
                                Cooling_power_consumption = readValue(row, 51, ref sheet),
                                Power_consumption_for_heating = readValue(row, 52, ref sheet),
                                Minimum_noise_level = readValue(row, 53, ref sheet),
                                Maximum_noise_level = readValue(row, 54, ref sheet),
                                Dimensions_indoor_unit_split_system = readValue(row, 55, ref sheet),
                                Dimensions_outdoor_unit_split_system = readValue(row, 56, ref sheet),
                                Maximum_length_of_communications = readValue(row, 57, ref sheet),
                                Minimum_room_temperature = readValue(row, 58, ref sheet),
                                Maximum_room_temperature = readValue(row, 59, ref sheet),
                                Minimum_temperature_in_cooling_mode = readValue(row, 60, ref sheet),
                                Minimum_temperature_in_heating_mode = readValue(row, 61, ref sheet),
                                Static_pressure = readValue(row, 62, ref sheet),
                                Phase = readValue(row, 63, ref sheet),
                                Weight_outdoor = readValue(row, 64, ref sheet),
                                Weight_of_inner = readValue(row, 65, ref sheet),
                                Display_indoor_unit = readValue(row, 66, ref sheet),
                                Power_supply = readValue(row, 67, ref sheet),
                                url = readValue(row, 68, ref sheet),
                                Refrigerant = readValue(row, 69, ref sheet),
                                Air_exchange = readValue(row, 70, ref sheet),
                                Air_consumption = readValue(row, 71, ref sheet),
                                Noise_level_external_unit = readValue(row, 72, ref sheet),
                                Noise_level_Internal_unit = readValue(row, 73, ref sheet),
                                Temperature_adjustment = readValue(row, 74, ref sheet),
                                Fan_Performance = readValue(row, 75, ref sheet),
                                url_yandex = readValue(row, 76, ref sheet),
                                EER = readValue(row, 77, ref sheet),
                                COP = readValue(row, 78, ref sheet),
                                class_energy = readValue(row, 79, ref sheet)
                        };

                            list.Add(p);
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] {0}", e);
                    throw e;
                }

                Console.WriteLine("[INFO] Прочитано {0} из {1} строк", list.Count, sheet.Dimension.End.Row - 1);
            }


            return list;
        }

        public static void makeNewFile()
        {
            List<Product> list = new List<Product>();

            // Формирование нового файла, готового для дополнения донорами
            Dictionary<string, Product> splits = readSplitList();
            foreach (KeyValuePair<string, Product> p in splits) list.Add(p.Value);
            WriteXslx(list, "default.xlsx");
        }
    }
}
