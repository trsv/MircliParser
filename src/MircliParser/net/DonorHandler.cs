﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using MircliParser.data;
using MircliParser.net.donor;
using MircliParser.io;
using System.IO;

namespace MircliParser.net
{
    /// <summary>
    /// Контейнер для сайтов-доноров
    /// </summary>
    class DonorHandler
    {
        /// <summary>
        /// Прогресс в процентах
        /// </summary>
        public int progressPercent { get; set; } = 0;

        /// <summary>
        /// Прогресс в текстовом сообщении
        /// </summary>
        public string progressMessage { get; set; }

        public Task process(IProgress<DonorHandler> progress)
        {


            return Task.Run(() =>
            {
                this.progressMessage = "Загружаем данные...";
                progress.Report(this);

                // список продуктов
                List<Product> list = ProductIO.ReadXlsx();
                //List<Product> list = new List<Product>(); list.Add(new Product { url_yandex = "https://market.yandex.ru/product/1733262602/spec?track=tabs" });

                // доноры
                DonorInterface siteMircli = new MircliDonor();
                DonorInterface siteTecl = new ParserTecl();
                DonorInterface siteYandex = new YandexDonor();
                DonorInterface siteProComfort = new ProComfortDonor();
                DonorInterface siteMirKlimata = new MirKlimataDonor();

                this.progressMessage = "Парсим...";
                progress.Report(this);

                // проходимся по всему списку ссылок
                int count = 0;
               
                list.ForEach(product =>
                {
                    this.progressMessage = "Парсим " + product.product_name;
                    this.progressPercent = ++count * 100 / list.Count;
                    progress.Report(this);

                    try
                    {
                        if (!product.url.Contains("mircli.ru")) return;

                        if (!string.IsNullOrWhiteSpace(product.EER))
                        {
                            Console.WriteLine("before: {0}", product.EER);
                            product.EER = product.EER.Trim();
                            Console.WriteLine("after: {0}", product.EER);

                            Console.WriteLine();
                        }
                        if (!string.IsNullOrWhiteSpace(product.COP)) product.COP = product.COP.Trim();

                        
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw e;
                    }
                });



                // выгрузка всего списка тиражей
                this.progressPercent = 100;
                this.progressMessage = "Сохраняем...";
                progress.Report(this);

                ProductIO.WriteXslx(list);

                this.progressMessage = "Готово";
                progress.Report(this);

            });
        }
        

        
    }
}
