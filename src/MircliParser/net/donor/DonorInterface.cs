﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using System.IO;
using System.IO.Compression;
using System.Net.Http;

using MircliParser.data;
using System.Drawing;
using System.Drawing.Imaging;
using Noesis.Drawing.Imaging.WebP;

namespace MircliParser.net.donor
{
    abstract class DonorInterface
    {
        public const string USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";

        /// <summary>
        /// Парсит страницу по указанной ссылке в url и возвращает объект продукт
        /// </summary>
        /// <param name="url">Ссылка на листинг продукта</param>
        /// <returns>Объект продукта</returns>
        abstract public void parse(ref Product product);

        public async Task<string> GetPageHtml(string url)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    Uri uri = new Uri(url);

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
                    client.DefaultRequestHeaders.Add("Host", uri.Host);
                    client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");

                    var response = await client.GetAsync(new Uri(url)).ConfigureAwait(false);

                    response.EnsureSuccessStatusCode();
                    using (var content = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        using (var decompressedStream = new GZipStream(content, CompressionMode.Decompress))
                        using (var streamReader = new StreamReader(decompressedStream))
                        {
                            return await streamReader.ReadToEndAsync().ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        
    }

    /// <summary>
    /// Набор дополнительных методов для обработки данных
    /// </summary>
    internal static class Utils
    {

        private const string PATH_IMAGE = @"image_new/";

        public static string extract(this string value, string regex, int group = 1)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value)) return string.Empty;
                else return Regex.Match(value, regex).Groups[group].Value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string clear(this string source, string @regex)
        {
            return Regex.Replace(source, regex, string.Empty).Trim();
        }

        public static int randomPause(int a, int b)
        {
            int pause;
            try
            {
                pause =  new Random().Next(a, b);
            }
            catch (Exception)
            {
                pause = 30;
            }

            pause *= 1000;

            Console.WriteLine("[INFO] Thread pause: {0}", pause);

            return pause;
        }

        public static string getWriteReadyName(this string name, bool rename, int number = 0)
        {
            if (string.IsNullOrWhiteSpace(name)) return string.Empty;

            //if (rename && name.Contains("avatars.mds.yandex.net")) return string.Empty;

            string new_name = Regex.Replace(name, @"[^а-яА-Яa-zA-Z0-9]", "_");
            if (number != 0) new_name += "_" + number.ToString();

            if (rename) new_name = PATH_IMAGE + new_name + ".jpg";

            return new_name;
        }

        public static void renameImages(this Product product)
        {
            if (!string.IsNullOrWhiteSpace(product.product_full_image1))
                product.product_full_image1 = product.product_name.getWriteReadyName(true, 1);

            if (!string.IsNullOrWhiteSpace(product.product_full_image2))
                product.product_full_image2 = product.product_name.getWriteReadyName(true, 2);

            if (!string.IsNullOrWhiteSpace(product.product_full_image3))
                product.product_full_image3 = product.product_name.getWriteReadyName(true, 3);

            if (!string.IsNullOrWhiteSpace(product.product_full_image4))
                product.product_full_image4 = product.product_name.getWriteReadyName(true, 4);

            if (!string.IsNullOrWhiteSpace(product.product_full_image5))
                product.product_full_image5 = product.product_name.getWriteReadyName(true, 5);
        }

        public static void downloadImages(this Product product)
        {
            Directory.CreateDirectory(PATH_IMAGE);

            bool success = false;

            Console.WriteLine("\n[INFO] Продукт: {0}", product.product_name);

            if (!string.IsNullOrWhiteSpace(product.product_full_image1))
                //downdloadImage(product.product_full_image1, product.product_name.getWriteReadyName(1));
            success = downdloadImage(product.product_full_image1, product.product_name.getWriteReadyName(false, 1)).Result;

            if (!string.IsNullOrWhiteSpace(product.product_full_image2))
                //downdloadImage(product.product_full_image2, product.product_name.getWriteReadyName(2));
            success = downdloadImage(product.product_full_image2, product.product_name.getWriteReadyName(false, 2)).Result;

            if (!string.IsNullOrWhiteSpace(product.product_full_image3))
                //downdloadImage(product.product_full_image3, product.product_name.getWriteReadyName(3));
            success = downdloadImage(product.product_full_image3, product.product_name.getWriteReadyName(false, 3)).Result;

            if (!string.IsNullOrWhiteSpace(product.product_full_image4))
                //downdloadImage(product.product_full_image4, product.product_name.getWriteReadyName(4));
                success = downdloadImage(product.product_full_image4, product.product_name.getWriteReadyName(false, 4)).Result;

            if (!string.IsNullOrWhiteSpace(product.product_full_image5))
                //downdloadImage(product.product_full_image5, product.product_name.getWriteReadyName(5));
                success = downdloadImage(product.product_full_image5, product.product_name.getWriteReadyName(false, 5)).Result;

            Console.WriteLine("[INFO] Скачаны все картинки: {0}", success);
        }

        private static async Task <Boolean> downdloadImage(string url, string name)
        {
            try
            {
                name = PATH_IMAGE + name + ".jpg";

                using (HttpClient client = new HttpClient())
                {
                    Console.WriteLine("[INFO] Скачиваем картинку: {0}", name);

                    Uri uri = new Uri(url);

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("User-Agent", DonorInterface.USER_AGENT);
                    client.DefaultRequestHeaders.Add("Host", uri.Host);

                    var response = await client.GetAsync(uri);

                    if (url.Contains("avatars.mds.yandex.net"))
                    {
                        string t = name + "-temp.webp";

                        var bytes = await response.Content.ReadAsByteArrayAsync();
                        File.WriteAllBytes(t, bytes);
                        using (Bitmap image = WebPFormat.LoadFromStream(new FileStream(t, FileMode.Open, FileAccess.Read)))
                        {
                            image.Save(name, ImageFormat.Jpeg);
                        }
                    }
                    else
                    {
                        var bytes = await response.Content.ReadAsStreamAsync();
                        Image image = Image.FromStream(bytes);
                        image.Save(name, ImageFormat.Jpeg);
                    }
                    
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] Не удалось скачать изображение по ссылке: {0}", url);
                Console.WriteLine(e);
                Console.WriteLine();

                return false;
            }
        }
    }
}
