﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.IO.Compression;
using System.Net.Http;
using HtmlAgilityPack;

using MircliParser.data;

namespace MircliParser.net.donor
{
    class MirKlimataDonor : DonorInterface
    {
        private HtmlDocument doc { get; set; } = new HtmlDocument();

        public override void parse(ref Product product)
        {

            Console.WriteLine("\n[INFO] Parsing: {0}", product.url);

            doc.LoadHtml(GetPageHtml(product.url).Result);
            if (doc.DocumentNode == null) throw new Exception("Не удалось получить страницу");

            try
            {
                parseData(ref product);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[INFO] Parsing: {0}", product.url);
                Console.WriteLine(e);
            }
        }

        private string getValue(string key)
        {
            string value = string.Empty;

            try
            {
                foreach (HtmlNode tr in doc.DocumentNode.SelectNodes("//div[@class='characteristic']/table/tbody/tr"))
                {
                    if (tr.SelectSingleNode(".//th").InnerText.Contains(key))
                    {
                        value = tr.SelectSingleNode(".//td").InnerText.Trim();
                        break;
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

            return value;
        }

        private void parseData(ref Product product)
        {
            product.EER = getValue("EER");
            product.COP = getValue("COP");

            product.class_energy = getValue("Класс энергоэффективности (охлаждение)");
            if (string.IsNullOrWhiteSpace(product.class_energy))
                product.class_energy = getValue("Класс энергоэффективности (обогрев)");

            product.contry_brand = getValue("Страна производителя");
            product.Manufacturer_country = getValue("Страна сборки");
            product.basic_model = getValue("Режим работы");
            product.square = getValue("Рекомендуемая площадь помещения");
            product.Inverter = getValue("Инверторное управление");

            product.Cooling_powerKBT = getValue("Производительность (охлаждение)");
            product.heating_capacityKBT = getValue("Производительность (обогрев)");

            product.Cooling_power_consumption = getValue("Потребляемая мощность (охлаждение)");
            product.Power_consumption_for_heating = getValue("Потребляемая мощность (обогрев)");

            product.Minimum_noise_level = getValue("Уровень звукового давления внутреннего блока").extract(@"(\d+)\/");
            product.Maximum_noise_level = getValue("Уровень звукового давления внутреннего блока").extract(@"\/(\d+)");

            product.Dimensions_indoor_unit_split_system = getValue("Габаритные размеры (внутренний блок)");
            product.Dimensions_outdoor_unit_split_system = getValue("Габаритные размеры (наружный блок)");

            product.Weight_outdoor = getValue("Вес (внутренний блок)");
            product.Weight_of_inner = getValue("Вес (наружный блок)");

            product.Phase = getValue("Электропитание").extract(@"^(\d+)");

            product.Maximum_length_of_communications = getValue("Максимальная длина труб хладагента");

            product.Remote_control = getValue("Пульт управления	");

            product.Refrigerant = getValue("Тип хладагента");

            product.Power_supply =  getValue("Тип блока");

            product.Number_indoor_units = getValue("Максимальное количество внутренних блоков");

            product.Minimum_temperature_in_cooling_mode = getValue("Диапазон температур наружного воздуха (охлаждение)").extract(@"([-|\+\w]+)");
            product.Minimum_temperature_in_heating_mode = getValue("Диапазон температур наружного воздуха (обогрев)").extract(@"([-|\+\w]+)");

            product.Indoor_unit_type = getValue("Тип внутреннего блока");

            product.Noise_level_external_unit = getValue("Уровень звукового давления");
            product.Noise_level_Internal_unit = getValue("Уровень звукового давления");


            if (string.IsNullOrWhiteSpace(product.product_desc))
            {
                HtmlNode desc = doc.DocumentNode.SelectSingleNode("//div[@class='marginLeft10']");
                product.product_desc = desc.InnerText.clear("Руководство по.*");
            }

            try
            {
                if (string.IsNullOrWhiteSpace(product.sale))
                {
                    product.sale = doc.DocumentNode.SelectSingleNode("//span[contains(@class, 'priceVariant')]").InnerText.clear(@"\s");
                    product.product_currency = "RUB";
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("[WARNING] No price");
            }

            try
            {
                HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//div[contains(@style, 'display: none')]/img");
                product.product_full_image1 = images[0].Attributes["src"].Value;
                product.product_full_image2 = images[1].Attributes["src"].Value;
                product.product_full_image3 = images[2].Attributes["src"].Value;
                product.product_full_image4 = images[3].Attributes["src"].Value;
                product.product_full_image5 = images[4].Attributes["src"].Value;
            }
            catch (ArgumentOutOfRangeException)
            {
                return;
            }
        }
    }
}
