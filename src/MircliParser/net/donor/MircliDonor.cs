﻿using System;
using System.Net;
using System.Net.Http;
using System.Windows.Forms;
using System.Web;

using HtmlAgilityPack;
using MircliParser.data;

namespace MircliParser.net.donor
{
    /// <summary>
    /// Класс для парсинга https://mircli.ru/
    /// </summary>
    class MircliDonor : DonorInterface
    {
        /// <summary>
        /// Список куков
        /// </summary>
        CookieContainer cookies = new CookieContainer();

        private const string HOST = "mircli.ru";

        private HtmlAgilityPack.HtmlDocument doc { get; set; } = new HtmlAgilityPack.HtmlDocument();




        /// <summary>
        /// Главный метод парсинга
        /// </summary>
        /// <param name="url">Ссылка на листинг продукта</param>
        /// <returns>Продукт</returns>
        public override void parse(ref Product product)
        {
            doc.LoadHtml(GetPageHtml(product.url).Result);

            //if (string.IsNullOrWhiteSpace(product.sale))
            //{
            //    product.sale = GetPrice();
            //    product.product_currency = "RUB";
            //}

            product.EER = GetValue("EER");
            product.COP = GetValue("COP");

            //product.contry_brand = GetValue("Страна");
            //product.Manufacturer_country = GetValue("Производитель");
            //product.Cooling_powerKBT = GetValue("Охлаждение");
            //product.heating_capacityKBT = GetValue("Обогрев");
            //product.square = GetValue("Площадь");
            //product.Inverter = GetValue("Компрессор");
            //product.Cooling_power_consumption = GetValue("Потребляемая мощность охлаждения");
            //product.Power_consumption_for_heating = GetValue("Потребляемая мощность обогрева");
            //product.Dimensions_outdoor_unit_split_system = GetValue("Габариты внеш. блока");
            //product.Dimensions_indoor_unit_split_system = GetValue("Габариты внут. блока");
            //product.basic_model = GetValue("Режимы работы"); 
            //product.Night_mode = GetValue("Ночной режим");
            //product.Maximum_length_of_communications = GetValue("Длина трасс");
            //product.Autoreset = GetValue("Авторестарт");
            //product.Fine_filter = GetValue("Фильтр тонкой очистки");
            //product.Self_diagnosis = GetValue("Самодиагностика");
            //product.air_ventilation_mode = GetValue("Режим приточной вентиляции");
            //product.Dimensions_indoor_unit_split_system = GetValue("Габариты внут. блока");
            //product.category_powerBTU = GetValue("Охлаждающая способность");
            //product.Remote_control = GetValue("Пульт");
            //product.Refrigerant = GetValue("Хладагент");
            //product.Display_indoor_unit = GetValue("Дисплей");
            //product.avto_temperatures = GetValue("Авто режим");
            //product.Phase = GetValue("Напряжение");
            //product.Weight_outdoor = GetValue("Вес внеш");
            //product.Weight_of_inner = GetValue("Вес внутр");
            //product.product_desc = GetDesc("Описание");
            //product.Air_exchange = GetValue("Воздухообмен");
            //product.Air_consumption = GetValue("Расход воздуха");
            //GetImage(ref product);
            //product.Dimensions_indoor_unit_split_system = GetValueBlok("Внутренний блок", "Габариты (ВхШхГ), см");
            //product.Dimensions_outdoor_unit_split_system = GetValueBlok("Внешний блок", "Габариты (ВхШхГ), см");
            //if (String.IsNullOrEmpty(product.Dimensions_indoor_unit_split_system) || String.IsNullOrEmpty(product.Dimensions_outdoor_unit_split_system))
            //{
                
            //    product.Dimensions_indoor_unit_split_system = GetValueBlok("Внутренний блок", "Габариты (ВхШхГ), см");
            //    product.Dimensions_outdoor_unit_split_system = GetValueBlok("Внешний блок", "Габариты (ВхШхГ), см");
            //}
            //product.Noise_level_Internal_unit = GetValueBlok("Внутренний блок", "Уровень шума, дБа");
            //product.Noise_level_external_unit = GetValueBlok("Внешний блок", "Уровень шума, дБа");
            //if (String.IsNullOrEmpty(product.Weight_of_inner) || String.IsNullOrEmpty(product.Weight_outdoor))
            //{
            //    product.Weight_outdoor = GetValueBlok("Внешний блок", "Вес, кг");
            //    product.Weight_of_inner = GetValueBlok("Внутренний блок", "Вес, кг");
            //}
            Console.WriteLine("[INFO] Парсим: {0}", doc.DocumentNode.InnerText.Length);

        }

        private string GetDesc(string key)
        {
            string res = "";
            try
            {
                string opis = key;
                int start = doc.DocumentNode.InnerText.IndexOf(opis) + opis.Length;
                res = doc.DocumentNode.InnerText.Substring(start, doc.DocumentNode.InnerText.IndexOf("Подробнее", start) - start);

                return HttpUtility.HtmlDecode(res);
            }
            catch(Exception ex)
            {

            }
            return HttpUtility.HtmlDecode(res);
        }

        private void GetImage(ref Product product)
        {
            try
            {
                HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//div[contains(@id, 'fotorama')]/img");
                product.product_full_image1 = "https://mircli.ru" + images[0].Attributes["src"].Value;
                product.product_full_image2 = "https://mircli.ru" + images[1].Attributes["src"].Value;
                product.product_full_image3 = "https://mircli.ru" + images[2].Attributes["src"].Value;
                product.product_full_image4 = "https://mircli.ru" + images[3].Attributes["src"].Value;
                product.product_full_image5 = "https://mircli.ru" + images[4].Attributes["src"].Value;
            }
            catch (Exception)
            {
                return;
            }
        }

        public string GetValue(string key)
        {
            string result = "";
            key = key.ToLower();

            try
            {
                HtmlNode div = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'pane') and contains(@id, '2')]");
                foreach (HtmlNode node in div.SelectNodes(".//li"))
                {
                    try
                    {
                        if (node.SelectSingleNode(".//span[@class='main']").InnerText.ToLower().Contains(key))
                        {
                            result = node.SelectSingleNode(".//span[@class='page']").InnerText;
                            
                            break;
                        }

                        
                    }
                    catch { }
                }

                Console.WriteLine("{0} = {1}", key, result);

            }
            catch (Exception)
            {
                result = string.Empty;
                //MessageBox.Show(exception.Message, "Ошибка");
            }

            return result;
        }

        public string GetValueBlok(string key1, string key2)
        {
            string result = "";
            try
            {
                string str2 = "Габариты, см", str3 = "Уровень шума, дБ", str4 = "Габариты внут. блока, (В*Ш*Г), см", str5 = "Габариты внут. блока, (В*Ш*Г)", str6 = "Габариты внеш. блока(В*Ш*Г)" ;
                int start = 0;
                try
                {
                    start = doc.DocumentNode.InnerText.IndexOf(key2, doc.DocumentNode.InnerText.IndexOf(key1)) + key2.Length;
                }
                catch(Exception ex)
                {

                }
                if(((start - key2.Length) == -1) && key2.Contains("Габариты"))
                    start = doc.DocumentNode.InnerText.IndexOf(str2, doc.DocumentNode.InnerText.IndexOf(key1)) + str2.Length;
                if (((start - str2.Length) == -1) && key2.Contains("Габариты"))
                    start = doc.DocumentNode.InnerText.IndexOf(str4, doc.DocumentNode.InnerText.IndexOf(key1)) + str4.Length;
                if (((start - str4.Length) == -1) && key2.Contains("Габариты") && key1.Contains("Внутренний блок"))
                    start = doc.DocumentNode.InnerText.IndexOf(str5, doc.DocumentNode.InnerText.IndexOf(key1)) + str5.Length;
                if (((start - str5.Length) == -1) || start == 0 && key2.Contains("Габариты") && key1.Contains("Внешний блок"))
                    start = doc.DocumentNode.InnerText.IndexOf(str6, doc.DocumentNode.InnerText.IndexOf(key1)) + str6.Length;

                if (((start - key2.Length) == -1) && key2.Contains("Уровень шума"))
                    start = doc.DocumentNode.InnerText.IndexOf(str3, doc.DocumentNode.InnerText.IndexOf(key1)) + str3.Length;

                int end = 0;
                string str = doc.DocumentNode.InnerText;

                if (key1.Contains("Внешний блок") && key2.Contains("Габариты") || key1.Contains("Внутренний блок") && key2.Contains("Габариты"))
                {
                    end = str.IndexOf("Вес", start);
                    result = doc.DocumentNode.InnerText.Substring(start, end - start);
                    if (result.Contains("Габариты"))
                        result = result.Substring(0, result.IndexOf("Габариты"));
                    if (result.Contains("Кондиционер"))
                        result = "";    
                }
                if (key1.Contains("Внешний блок") && key2.Contains("Уровень шума") || key1.Contains("Внутренний блок") && key2.Contains("Уровень шума"))
                {
                    end = str.IndexOf("Габариты", start);
                    result = doc.DocumentNode.InnerText.Substring(start, end - start).Replace("?","")/*.extract(@"(\d+)")*/;
                    if (result.Contains("Москве"))
                        result = "";
                }
                
                if (key1.Contains("Внешний блок") && key2.Contains("Вес"))
                { 
                    end = str.IndexOf("Внутренний", start-1);
                    result = doc.DocumentNode.InnerText.Substring(start, end - start).extract(@"(\d+)");
                }
                if (key1.Contains("Внутренний блок") && key2.Contains("Вес"))
                {
                    end = str.IndexOf("Страна", start - 1);
                    result = doc.DocumentNode.InnerText.Substring(start, end - start).extract(@"(\d+)");

                }
                


                return result.Replace("\r\n", "").Replace(" ", "");
            }
            catch(Exception ex)
            {
            }
            
            return result.Replace("\r\n", "").Replace(" ", "");
        }

        private string GetPrice()
        {
            string res = "";
            try
            {
                string str = doc.DocumentNode.InnerText;
                string opis = "price\": \"";
                int start = doc.DocumentNode.InnerHtml.IndexOf(opis) + opis.Length;
                res = doc.DocumentNode.InnerHtml.Substring(start, doc.DocumentNode.InnerHtml.IndexOf("\"", start) - start);
                

                return HttpUtility.HtmlDecode(res);
            }
            catch (Exception ex)
            {

            }

            return HttpUtility.HtmlDecode(res);
        }

    }
}
