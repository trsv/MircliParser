﻿using System;
using MircliParser.data;
using System.Net;
using System.Web;

using HtmlAgilityPack;

namespace MircliParser.net.donor
{
    class ParserTecl : DonorInterface
    {
        CookieContainer cookies = new CookieContainer();

        private const string HOST = "tecl.ru";

        private HtmlAgilityPack.HtmlDocument doc { get; set; } = new HtmlAgilityPack.HtmlDocument();

        public override void parse(ref Product product)
        {
            try
            {
                doc.LoadHtml(GetPageHtml(product.url).Result);

                if (doc.DocumentNode == null) throw new Exception("Не удалось получть страницу");

                if (string.IsNullOrWhiteSpace(product.sale))
                {
                    product.sale = GetPrice();
                    if (!string.IsNullOrWhiteSpace(product.sale)) product.product_currency = "RUB";
                }


                product.Inverter = GetValue("Инвертор");
                product.basic_model = GetValue("Режим работы");

                product.Cooling_powerKBT = (Convert.ToDouble(GetValue("Мощность охлаждения")) / 1000.0).ToString().Replace(".", ",");
                product.heating_capacityKBT = (Convert.ToDouble(GetValue("Мощность обогрева")) / 1000.0).ToString().Replace(".", ",");

                product.Indoor_unit_type = GetValue("Тип оборудования");
                product.Refrigerant = GetValue("Хладагент");
                product.Autoreset = GetValue("Автоматический перезапуск");
                product.Timer = GetValue("Таймер включения/выключения");
                product.Remote_control = GetValue("Пульт ДУ");

                product.Minimum_temperature_in_cooling_mode = GetValue("При охлаждении").extract(@"([-|\+\d]+)");
                product.Minimum_temperature_in_heating_mode = GetValue("При обогреве").extract(@"([-|\+\d]+)");

                product.Minimum_noise_level = GetValue("Уровень шума внутреннего блока").extract(@"(\d+)");
                product.Maximum_noise_level = GetValue("Уровень шума внутреннего блока").extract(@"(\d+)", 2);

                product.Fan_Performance = GetValue("Производительность вентилятора");

                product.Cooling_power_consumption = GetValue("Потребляемая мощность, охлаждение");
                product.Power_consumption_for_heating = GetValue("Потребляемая мощность, обогрев");

                product.Phase = GetValue("Параметры электропитания").extract(@"(\d+)");

                product.Dimensions_indoor_unit_split_system = 
                    GetValue("Ширина, внутренний блок") + "x" + GetValue("Высота, внутренний блок") + "x" + GetValue("Глубина, внутренний блок");
                if (product.Dimensions_indoor_unit_split_system.Equals("xx")) product.Dimensions_indoor_unit_split_system = string.Empty;

                product.Weight_of_inner = GetValue("Вес, внутренний блок").Replace(".", ",");
                product.Weight_outdoor = GetValue("Вес, наружный блок").Replace(".", ",");

                product.Dehumidification_mode = GetValue("Режим осушения");
                product.avto_temperatures = GetValue("Автоматический режим");
                product.Night_mode = GetValue("Ночной режим");
                product.Self_diagnosis = GetValue("Самодиагностика неисправностей");
                product.Fine_filter = GetValue("Фильтр тонкой очистки");

                product.Adjusting_air_flow = GetValue("Регулировка силы воздушного потока");
                if (string.IsNullOrWhiteSpace(product.Adjusting_air_flow))
                    product.Adjusting_air_flow = GetValue("Регулировка направления воздушного потока");

                product.Temperature_adjustment = GetValue("Регулировка температуры");

                product.Noise_level_external_unit = GetValue("Уровень шума наружного блока");
                product.Noise_level_Internal_unit = GetValue("Уровень шума внутреннего блока");

                product.Dimensions_outdoor_unit_split_system =
                    GetValue("Ширина, наружный блок") + "x" + GetValue("Высота, наружный блок") + "x" + GetValue("Глубина, наружный блок");
                if (product.Dimensions_outdoor_unit_split_system.Equals("xx")) product.Dimensions_outdoor_unit_split_system = string.Empty;

                product.Weight_outdoor = GetValue("Вес, наружный блок");
                product.square = GetValue("Площадь помещения");
                product.Manufacturer_country = GetValue("Страна производства");
                product.class_energy = GetValue("Класс энергопотребления");
                product.Ventilation_mode = GetValue("Режим вентилятора");
                product.Display_indoor_unit = GetValue("Дисплей");
                product.Indoor_unit_type = GetValue("Тип блока");
                product.Deodorizing_filter = GetValue("Дезодорирующий фильтр");
                product.Plasma_filter = GetValue("Плазменный фильтр");
                product.Maximum_air_flow = GetValue("Поток воздуха");
                      
                product.product_desc = GetDesc();
                
                GetImage(ref product);
            }
            catch(Exception)
            {

            }

        }

        private string GetSubStr(string str, string key1, string key2)
        {
            /*
             * Значение "LL" обозначает чтобы использовался метод LastIndexOf
             * Значение "FF" обозначает чтобы использовался метод IndexOf
             * */
            try
            {
                if (key1.Contains("LL"))
                    return str.Substring(str.LastIndexOf(key2) + 1);
                else if (key1.Contains("FF"))
                    return str.Substring(str.IndexOf(key2) + 1);
                return str;
            }
            catch(Exception)
            {

            }

            return str;
        }

        private void GetImage(ref Product product)
        {
            try
            {
                HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//div[@id='main_image']/a");
                product.product_full_image1 = "http://tecl.ru" + images[0].SelectSingleNode(".//img").Attributes["src"].Value;
                product.product_full_image2 = "http://tecl.ru" + images[1].SelectSingleNode(".//img").Attributes["src"].Value;
                product.product_full_image3 = "http://tecl.ru" + images[2].SelectSingleNode(".//img").Attributes["src"].Value;
                product.product_full_image4 = "http://tecl.ru" + images[3].SelectSingleNode(".//img").Attributes["src"].Value;
                product.product_full_image5 = "http://tecl.ru" + images[4].SelectSingleNode(".//img").Attributes["src"].Value;
            }
            catch (ArgumentOutOfRangeException)
            {
                return;
            }
        }

        public string GetValue(string key)
        {
            string result = "";
            try
            {
                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//table[@class='fixoptions']/tr"))
                {
                    if (node.InnerText.Contains(key))
                        result = node.SelectSingleNode(".//td/b").InnerText;
                }
                return result;
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }

        private string GetDesc()
        {
            string res = "";
            try
            {
                res = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'box') and contains(@class, '500')]").InnerText;
                res = res.Replace("Описание товара", "");
                res = res.Trim();

                return HttpUtility.HtmlDecode(res);
            }
            catch (Exception)
            {

            }

            return HttpUtility.HtmlDecode(res);
        }

        private string GetPrice()
        {
            string res = "";
            try
            {
                res = doc.DocumentNode.SelectSingleNode("//span[contains(@class, 'text') and contains(@class, 'ar')]").InnerText;
                res = res.extract(@"(\d+)");

                return HttpUtility.HtmlDecode(res);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

    }
}
