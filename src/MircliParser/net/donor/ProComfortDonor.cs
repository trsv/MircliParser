﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HtmlAgilityPack;

using MircliParser.data;

namespace MircliParser.net.donor
{
    class ProComfortDonor : DonorInterface
    {
        private HtmlDocument doc { get; set; } = new HtmlDocument();

        public override void parse(ref Product product)
        {
            Console.WriteLine("\n[INFO] Parsing: {0}", product.url);

            doc.LoadHtml(GetPageHtml(product.url).Result);
            if (doc.DocumentNode == null) throw new Exception("Не удалось получить страницу");

            try
            {
                parseData(ref product);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[INFO] Parsing: {0}", product.url);
                Console.WriteLine(e);
            }
        }

        private string getValue(string key)
        {
            string value = string.Empty;

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                List<string> t_keys = new List<string>();
                List<string> t_values = new List<string>();

                HtmlNode div = doc.DocumentNode.SelectSingleNode("//dl[contains(@class, 'expand')]");

                foreach (HtmlNode dt in div.SelectNodes(".//dt")) t_keys.Add(dt.InnerText.Trim().ToLower());
                foreach (HtmlNode dd in div.SelectNodes(".//dd")) t_values.Add(dd.InnerText.Trim().clear(@"\s+"));

                for (int i = 0; i < t_keys.Count; i++)
                {
                    string t_key = t_keys[i];
                    string t_value = t_values[i];

                    data.Add(t_key, t_value);
                }

                key = key.ToLower();

                if (data.ContainsKey(key)) value = data[key];
                else value = string.Empty;

            }
            catch (Exception)
            {
                value = string.Empty;
            }

            return value;
        }

        private string getValueFromAdditional(string key, string group = "Дополнительная информация")
        {
            string additional = getValue(group).ToLower();

            bool enable = additional.Contains(key.ToLower());

            return enable ? "есть" : "нет";
        }

        private void parseData(ref Product product)
        {
            product.contry_brand = getValue("Страна производитель");
            product.Manufacturer_country = getValue("Сборка");

            product.basic_model = getValue("Режим работы кондиционера");
            product.Indoor_unit_type = getValue("Тип внутреннего блока");
            product.square = getValue("Обслуживаемая площадь");
            product.Inverter = getValue("Инвертор");

            product.Dimensions_indoor_unit_split_system = getValue("Габарит внутреннего блока").Replace("/", "x");
            product.Dimensions_outdoor_unit_split_system = getValue("Габарит наружного").Replace("/", "x");

            
            product.Cooling_powerKBT = getValue("Мощность охлаждения");
            product.heating_capacityKBT = getValue("Мощность обогрева");

            product.Phase = getValue("Электропитание").extract(@"\/(\d+)\/");

            product.Cooling_power_consumption = getValue("Потребляемая мощность");
            product.Power_consumption_for_heating = product.Cooling_power_consumption;

            product.Minimum_noise_level = getValue("Уровень шума").extract(@"(\d+)\/");
            product.Maximum_noise_level = getValue("Уровень шума").extract(@"\/(\d+)");

            product.Minimum_temperature_in_cooling_mode = 
                getValue("Диапазон температур наружного воздуха охлаждение").extract(@"([-|\+\w]+)");
            product.Minimum_temperature_in_heating_mode = 
                getValue("Диапазон температур наружного воздуха обогрев").extract(@"([-|\+\w]+)");

            product.Maximum_length_of_communications = getValue("Максимальная длина трассы");
            product.Night_mode = getValue("Ночной режим");
            product.Wi_Fi = getValue("wi-fi");
            product.avto_temperatures = getValue("Автоматический режим");
            product.Timer = getValue("Таймер включения/выключения");
            product.Remote_control = getValue("Пульт дистанционного управления");

            product.Self_diagnosis = getValue("Самодиагностика");
            if (string.IsNullOrWhiteSpace(product.Self_diagnosis))
                product.Self_diagnosis = getValueFromAdditional("самодиагностика неисправностей");

            product.Adjusting_air_flow = getValueFromAdditional("регулировки направления воздушного потока");
            product.Plasma_filter = getValueFromAdditional("плазменный фильтр");
            product.Deodorizing_filter = getValueFromAdditional("дезодорирующий фильтр");
            product.Anion_generator = getValueFromAdditional("генератор анионов");
            product.Ventilation_mode = getValueFromAdditional("режим вентиляции");
            product.avto_temperatures = getValueFromAdditional("автоматическое поддержание температуры");

            product.Fine_filter = getValue("Фильтры тонкой очистки воздуха");
            product.Autoreset = getValue("Автоперезапуск");
            product.Motion_Sensor = getValue("Датчик движения");
            product.Refrigerant = getValue("Хладогент");
            product.Dehumidification_mode = getValue("Режим осушения воздуха");
            product.Maximum_air_flow = getValue("Поток воздуха");


            try
            {
                if (string.IsNullOrWhiteSpace(product.sale))
                {
                    product.sale = doc.DocumentNode.SelectSingleNode("//span[contains(@class, 'price')]").Attributes["data-price"].Value;
                    product.product_currency = "RUB";
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("\n[WARNING] No price");
            }

            try
            {
                if (string.IsNullOrWhiteSpace(product.product_s_desc))
                    product.product_s_desc = doc.DocumentNode.SelectSingleNode("//div[@id='cat_description']").InnerText.Trim();
            }
            catch (Exception) { }

            try
            {
                if (string.IsNullOrWhiteSpace(product.product_desc))
                    product.product_desc = doc.DocumentNode.SelectSingleNode("//div[@id='description']").InnerText.Trim();
            }
            catch (Exception) { }

            try
            {
                //HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//div[contains(@style, 'display: none')]/img");
                //product.product_full_image1 = "http://pro-komfort.com" + images[0].Attributes["src"].Value;
                //product.product_full_image2 = images[1].Attributes["src"].Value;
                //product.product_full_image3 = images[2].Attributes["src"].Value;
                //product.product_full_image4 = images[3].Attributes["src"].Value;
                //product.product_full_image5 = images[4].Attributes["src"].Value;
            }
            catch (ArgumentOutOfRangeException)
            {
                return;
            }
        }
    }
}
