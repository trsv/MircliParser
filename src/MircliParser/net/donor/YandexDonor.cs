﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using System.IO.Compression;
using System.IO;
using System.Web;
using System.Net;
using System.Net.Http;
using HtmlAgilityPack;

using MircliParser.data;

namespace MircliParser.net.donor
{
    class YandexDonor : DonorInterface
    {
        private CookieContainer cookies = new CookieContainer();

        private HtmlAgilityPack.HtmlDocument doc { get; set; } = new HtmlAgilityPack.HtmlDocument();

        private string yandexProductId { get; set; }
        private string url_Yandex { get; set; }

        public YandexDonor()
        {
            if (!getCookies().Result)
            {
                Console.WriteLine("[ERROR] Yandex. Не удалось получить куки");
                throw new Exception("[ERROR] Не удалось получить куки");
            }
        }

        public override void parse(ref Product product)
        {
            try
            {
                //if (!search(product.product_name))
                //{
                //    Console.WriteLine("[WARNING] Skiped (can`t get id): {0}", product.product_name);
                //    return;
                //}

                //Console.WriteLine("[INFO] Yandex product id: {0}", yandexProductId);

                parseData(ref product);
                //parseCharacteristic(ref product);
            }
            catch (Exception e)
            {
                Console.WriteLine("[WARNING] Skiped (parse error): {0}", product.product_name);
                Console.WriteLine(e);
            }
        }

        private async Task<Boolean> getCookies()
        {
            cookies = new CookieContainer();

            using (HttpClientHandler handler = new HttpClientHandler { CookieContainer = cookies })
            using (HttpClient client = new HttpClient(handler))
            {
                Uri uri = new Uri("https://market.yandex.ru/");

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
                client.DefaultRequestHeaders.Add("Host", uri.Host);
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                client.DefaultRequestHeaders.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");

                await client.GetAsync(uri).ConfigureAwait(false);

                return true;
            }
        }

        private bool search(string title)
        {
            try
            {
                string pattern = "https://market.yandex.ru/search?cvredirect=2&text=%title%";
                string url = pattern.Replace("%title%", HttpUtility.UrlEncode(title));

                string html = loadPage(url).Result;
                
                doc.LoadHtml(html);

                return !string.IsNullOrWhiteSpace(yandexProductId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        private void selectFirstProductFromCatalog()
        {
            string url = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'card2__title')]/a")
                .Attributes["href"]
                .Value
                .ToString();

            url = "https://market.yandex.ru" + url;

            string html = loadPage(url).Result;
            doc.LoadHtml(html);
        }

        private async Task<string> loadPage(string url)
        {
            Thread.Sleep(Utils.randomPause(1, 4));
            
            try
            {
                using (HttpClientHandler handler = new HttpClientHandler
                {
                    AllowAutoRedirect = true,
                    CookieContainer = cookies,
                    UseCookies = true
                }
                )
                using (HttpClient client = new HttpClient(handler))
                {
                    Uri uri = new Uri(url);

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
                    client.DefaultRequestHeaders.Add("Host", uri.Host);
                    client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                    client.DefaultRequestHeaders.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
                    client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");

                    var response = await client.GetAsync(new Uri(url)).ConfigureAwait(false);

                    response.EnsureSuccessStatusCode();
                    using (var content = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        using (var decompressedStream = new GZipStream(content, CompressionMode.Decompress))
                        using (var streamReader = new StreamReader(decompressedStream))
                        {
                            yandexProductId = response.RequestMessage.RequestUri.ToString().extract(@"product\/(.*?)\?");
                            url_Yandex = response.RequestMessage.RequestUri.ToString();
                            return await streamReader.ReadToEndAsync().ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        private void parseData(ref Product product)
        {
            string url = product.url_yandex;
            doc.LoadHtml(loadPage(url).Result);

            // картинки
            try
            {
                HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//meta[@property='og:image']");
                if (string.IsNullOrWhiteSpace(product.product_full_image1))
                product.product_full_image1 = images[0].Attributes["content"].Value;

                if (string.IsNullOrWhiteSpace(product.product_full_image2))
                    product.product_full_image2 = images[1].Attributes["content"].Value;

                if (string.IsNullOrWhiteSpace(product.product_full_image3))
                    product.product_full_image3 = images[2].Attributes["content"].Value;

                if (string.IsNullOrWhiteSpace(product.product_full_image4))
                    product.product_full_image4 = images[3].Attributes["content"].Value;

                if (string.IsNullOrWhiteSpace(product.product_full_image5))
                    product.product_full_image5 = images[4].Attributes["content"].Value;
            }
            catch (ArgumentOutOfRangeException)
            {
                return;
            }
        }

        private void parseCharacteristic(ref Product product)
        {
            string url = product.url_yandex;
            doc.LoadHtml(loadPage(url).Result);

            if (string.IsNullOrWhiteSpace(product.url_yandex))
                product.url_yandex = url_Yandex;
            url_Yandex = "";


            if (string.IsNullOrWhiteSpace(product.class_energy))
                product.class_energy = getValue("Класс энергопотребления"); 

            if (string.IsNullOrWhiteSpace(product.Indoor_unit_type))
                product.Indoor_unit_type = getValue("Тип");

            if (string.IsNullOrWhiteSpace(product.Maximum_length_of_communications))
                product.Maximum_length_of_communications = getValue("Максимальная длина коммуникаций");

            if (string.IsNullOrWhiteSpace(product.basic_model))
                product.basic_model = getValue("Основные режимы");


            if (string.IsNullOrWhiteSpace(product.Maximum_air_flow))
                product.Maximum_air_flow = getValue("Максимальный воздушный поток");

            if (string.IsNullOrWhiteSpace(product.Static_pressure))
                product.Static_pressure = getValue("Статическое давление");


            if (string.IsNullOrWhiteSpace(product.Cooling_powerKBT))
                product.Cooling_powerKBT = computeKBT(getValue("Мощность в режиме охлаждения"));


            if (string.IsNullOrWhiteSpace(product.heating_capacityKBT))
                product.heating_capacityKBT = computeKBT(getValue("Мощность в режиме обогрева"));

            if (string.IsNullOrWhiteSpace(product.Cooling_power_consumption))
                product.Cooling_power_consumption = computeKBT(getValue("Потребляемая мощность при охлаждении"));

            if (string.IsNullOrWhiteSpace(product.Power_consumption_for_heating))
                product.Power_consumption_for_heating = computeKBT(getValue("Потребляемая мощность при обогреве"));

            if (string.IsNullOrWhiteSpace(product.Power_supply))
                product.Power_supply = getValue("Подача питания");

            if (string.IsNullOrWhiteSpace(product.air_ventilation_mode))
                product.air_ventilation_mode = getValue("Режим приточной вентиляции");



            // из "дополнительные режимы"
            if (string.IsNullOrWhiteSpace(product.avto_temperatures))
                product.avto_temperatures = getValueFromAdditional("автоматическое поддержание температуры");

            if (string.IsNullOrWhiteSpace(product.Self_diagnosis))
                product.Self_diagnosis = getValueFromAdditional("самодиагностика неисправностей");

            if (string.IsNullOrWhiteSpace(product.Night_mode))
                product.Night_mode = getValueFromAdditional("ночной режим");

            if (string.IsNullOrWhiteSpace(product.Ventilation_mode))
                product.Ventilation_mode = getValueFromAdditional("режим вентиляции (без охлаждения и обогрева)");




            if (string.IsNullOrWhiteSpace(product.Dehumidification_mode))
                product.Dehumidification_mode = getValue("Режим осушения");

            if (string.IsNullOrWhiteSpace(product.Remote_control))
                product.Remote_control = getValue("Пульт дистанционного управления");
            
            if (string.IsNullOrWhiteSpace(product.Timer))
                product.Timer = getValue("Таймер включения/выключения");

            if (string.IsNullOrWhiteSpace(product.Minimum_noise_level))
                product.Minimum_noise_level = getValue("Уровень шума внутреннего блока").extract(@"(\d+).*?\/");
            if (string.IsNullOrWhiteSpace(product.Maximum_noise_level))
                product.Maximum_noise_level = getValue("Уровень шума внутреннего блока").extract(@"\/.*?(\d+)");

            if (string.IsNullOrWhiteSpace(product.Refrigerant))
                product.Refrigerant = getValue("Тип хладагента");
            if (string.IsNullOrWhiteSpace(product.Fine_filter))
                product.Fine_filter = getValue("Фильтры тонкой очистки воздуха");
            if (string.IsNullOrWhiteSpace(product.Number_of_fan_speeds))
                product.Number_of_fan_speeds = getValue("Регулировка скорости вращения вентилятора").extract(@"(\d+)");



            // из "другие фунции и особенности"
            if (string.IsNullOrWhiteSpace(product.Adjusting_air_flow))
                product.Adjusting_air_flow = getValueFromAdditional("регулировки направления воздушного потока", "Другие функции и особенности");
            if (string.IsNullOrWhiteSpace(product.Deodorizing_filter))
                product.Deodorizing_filter = getValueFromAdditional("дезодорирующий фильтр", "Другие функции и особенности");
            if (string.IsNullOrWhiteSpace(product.Plasma_filter))
                product.Plasma_filter = getValueFromAdditional("плазменный фильтр", "Другие функции и особенности");
            if (string.IsNullOrWhiteSpace(product.Motion_Sensor))
                product.Motion_Sensor = getValueFromAdditional("датчик движения", "Другие функции и особенности");
            if (string.IsNullOrWhiteSpace(product.Filter_with_vitamin_C))
                product.Filter_with_vitamin_C = getValueFromAdditional("фильтр с витамином", "Другие функции и особенности");
            if (string.IsNullOrWhiteSpace(product.Anion_generator))
                product.Anion_generator = getValueFromAdditional("генератор анионов", "Другие функции и особенности");




            if (string.IsNullOrWhiteSpace(product.Minimum_temperature_in_cooling_mode))
                product.Minimum_temperature_in_cooling_mode = 
                getValue("Минимальная температура для эксплуатации кондиционера в режиме охлаждения").extract(@"(.*?) ");
            if (string.IsNullOrWhiteSpace(product.Minimum_temperature_in_heating_mode))
                product.Minimum_temperature_in_heating_mode = 
                getValue("Минимальная температура для эксплуатации кондиционера в режиме обогрева").extract(@"(.*?) ");

            if (string.IsNullOrWhiteSpace(product.Dimensions_indoor_unit_split_system))
                product.Dimensions_indoor_unit_split_system = getValue("Внутреннего блока сплит-системы или мобильного кондиционера");
            if (string.IsNullOrWhiteSpace(product.Dimensions_outdoor_unit_split_system))
                product.Dimensions_outdoor_unit_split_system = getValue("Наружного блока сплит-системы или оконного кондиционера");

            if (string.IsNullOrWhiteSpace(product.Weight_of_inner))
                product.Weight_of_inner = getValue("Вес внутреннего блока");
            if (string.IsNullOrWhiteSpace(product.Weight_outdoor))
                product.Weight_outdoor = getValue("Вес внешнего блока");

            if (string.IsNullOrWhiteSpace(product.Wi_Fi))
                product.Wi_Fi = getValue("Wi-Fi");

            if (string.IsNullOrWhiteSpace(product.square))
                product.square = getValue("Обслуживаемая площадь");

            if (string.IsNullOrWhiteSpace(product.Inverter))
                product.Inverter = getValue("Инвертор");

            if (string.IsNullOrWhiteSpace(product.Phase))
                product.Phase = getValue("Напряжение");

            if (string.IsNullOrWhiteSpace(product.Display_indoor_unit))
                product.Display_indoor_unit = getValue("Дисплей");

            if (string.IsNullOrWhiteSpace(product.Number_indoor_units))
                product.Number_indoor_units = getValue("Число внутренних блоков");


        }

        private string getValue(string key)
        {
            string value = string.Empty;

            try
            {
                foreach (HtmlNode tr in doc.DocumentNode.SelectNodes("//dl"))
                {
                    if (tr.SelectSingleNode(".//dt").InnerText.Contains(key))
                    {
                        value = tr.SelectSingleNode(".//dd").InnerText.Trim();
                        break;
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

            return value;
        }

        private string getValueFromAdditional(string key, string additioal = "Дополнительные режимы")
        {
            string additional = getValue(additioal).ToLower();

            bool enable = additional.Contains(key);

            return enable ? "есть" : "нет";
        }

        private string computeKBT(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                double kbt = Convert.ToDouble(value.extract(@"(\d+)"));
                kbt /= 1000.0;
                return kbt.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
